# labreins

Git and GitLab is great to control and manage your website source files. But for people cant handle git-cli and even the [GitLab Web Editor](http://doc.gitlab.com/ce/workflow/web_editor.html), there should be a edit button on the page they have in front of their eyes.

editors need: 

* an user on the used gitlab instance (GitLab.com or your own)
* write access to the repository and branch. The branch master is [protected](http://doc.gitlab.com/ce/workflow/protected_branches.html) by default, so unprotect it, or better use a patch branch.


Demo at [klml.gitlab.io](http://klml.gitlab.io/) (ask me for write access).


[GitLab CE feature request for this](https://gitlab.com/gitlab-org/gitlab-ce/issues/11070)


## Embedd

jQuery and [jquery-cookie](https://github.com/carhartl/jquery-cookie) is required.

```
<script src="//cdn.jsdelivr.net/g/jquery@2.2.3,jquery.cookie@1.4.1" type="text/javascript"></script>
```

The script must be [cors enabled](http://enable-cors.org). You can use `//labreins.klml.de/`.

```
<script src="//labreins.klml.de/labreins.js" type="text/javascript"></script>

<script>
var labreinsLocal = { "gitlab_id": "5", "gitlab_file_path": "{{ page.path }}" } // jekyll example

jQuery(document).ready(function() {
   (this.hashrouter = function () {
        if ( window.location.hash == "#labreins-edit" ) {
            $( "#labreins-edit" ).load( "//labreins.klml.de/labreins.html", function() {
                $.get('//labreins.klml.de/labreins.js');
            });
        }
   })();
   window.onhashchange = this.hashrouter;
});

</script>
```

With 'labreinsLocal' you can set:

labreins.gitlab_id
: [project ID](http://docs.gitlab.com/ce/api/projects.html) is __mandatory__

labreins.gitlab_file_path
: [Full path to file](http://docs.gitlab.com/ce/api/repository_files.html#get-file-from-repository) is __mandatory__ ( `{{ page.path }}` is a example [jekyll variable](https://jekyllrb.com/docs/variables/) 

labreins.gitlab_host
: your GitLab instance (default: gitlab.com)

labreins.labreins.gitlab_ref
: name of branch, tag or commit the existing source comes from (default: master)

labreins.gitlab_branch_name
: name of branch where to commit (default: master)


### edit button

Edit mode gets always activated with the URL-hash `#labreins-edit`. Optional you can help users with this edit link. 

```
       <div id="labreins-edit" >
            <a href="#labreins-edit" >edit</a>
        </div>
```


## Todo

* message: runner needs time
  * get status of build and show
* store token in cookie
* create new page from 404
* where to get id? 
  * get id automatic 
* uses [sessions](http://docs.gitlab.com/ce/api/session.html) instead of tokens, but I dont want to have users pass. Can I get the token from a loged in browser, without passwords? [current-user](http://docs.gitlab.com/ce/api/users.html#current-user) does not work without token
* hide unuseful formfields (textarea when ther is no _valid_ token, token when there is a valid token)
* load only one ressource in page, avoid this $.load and $.get construct
  * probelm, script cant load external file (want no workarounds with host as variable)
  * possible solutions: 1 file: script tag in html or html in script
* allow users to fork
