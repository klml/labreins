var labreins =  [] ;
labreins.gitlab_host= "//gitlab.com" ;


jQuery(document).ready(function() {

    // overwrite main variables (labreins) with values from config
    $.each( labreinsLocal , function( index, value ) {
        labreins[ index ] = value ;
    });

    // overwrite html link for token
    $('#labreins-tokensource').attr('href', labreins.gitlab_host + '/profile/account');

    // write parameter to form
    $('#labreins-file_path').val( labreins.gitlab_file_path );

    if (typeof labreins.gitlab_branch_name != 'undefined') {
        $( '#labreins-branch_name' ).val( labreins.gitlab_branch_name );
    };
    if (typeof labreins.gitlab_ref != 'undefined') {
        $( '#labreins-ref' ).val( labreins.gitlab_ref );
    };

    // combinations
    labreins.apiurl = labreins.gitlab_host + "/api/v3/projects/" + labreins.gitlab_id + "/repository/files" ;


    // user has to give his token before loading current source text

    if (typeof $.cookie('labreins-private_token') != 'undefined') {
        $( "#labreins-private_token" ).val( $.cookie('labreins-private_token') ) ;
        labreinsGetText () ;
    };

    $( "#labreins-private_token" ).blur(function() {
        labreinsGetText ( $( this ).val() );
        $.cookie('labreins-private_token', this.value , { path: '/' });
    });

    labreinsSave( );
});


function labreinsGetText ( token ) {

    $.ajax({
        url: labreins.apiurl ,
        type: "GET" ,
        data: $( '#labreins-form').serialize() ,
        success: function( content ){
            $('#labreins-form textarea').text( atob( content.content ) );
            window.location.hash = "#labreins-edit"
        },
        error:function( msg ){
            $( '#errormsg' ).show( function() {
                 $(this).find('pre').text( msg );
            });
        }
    });
}
function labreinsSave ( ) {
    $('#labreins-save').click( function() { // assign submit after changing the editor by user
        $( '#labreins-form' ).submit( function(event) {
    
            event.preventDefault();
            $.ajax({
                url: labreins.apiurl ,
                type: "PUT" ,
                data: $(this).serialize() ,
                success: function( msg ){
                    $( '#successmsg' ).show("slow", function() {
                        $(this).find('pre').text( 'File ' + msg.file_path + ' written on branch ' + msg.branch_name + '. Check your Gitlab builds for the new page.' );
                        window.location.hash = "#main"
                        //~ location.reload();
                    });
                },
                error:function( msg ){
                    $( '#errormsg' ).show( function() {
                         $(this).find('pre').text( msg.statusText );
                    });
                }
            });         
        });    
    });
}
